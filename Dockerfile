# trinitycore-runtime
FROM docker.io/debian

# TODO: Put the maintainer name in the image metadata
LABEL maintainer="Fulvio Carrus <f.carrus@gmail.com>"

# TODO: Rename the builder environment variable to inform users about application you provide them
ENV TRINITYCORE_RUNTIME_VERSION 1.0

# TODO: Set labels used in OpenShift to describe the builder image
LABEL io.k8s.description="Platform for running TrinityCore 3.3.5" \
      io.k8s.display-name="TrinityCore 3.3.5 Runtime" \
      io.openshift.tags="runtime,trinitycore" 
#      io.openshift.s2i.assemble-input-files="/opt/trinity/bin;/opt/trinity/etc;/opt/trinity/sql;/opt/trinity/TDB_full_world_335.64_2018_02_19.sql" \
#      io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

RUN apt-get update && \
    apt-get -y install \
      libncurses5 \
      libreadline7 \
      libboost-filesystem1.62 \
      libboost-thread1.62 \
      libboost-program-options1.62 \
      libboost-iostreams1.62 \
      libssl1.0.2 \
      libmariadbclient18 \
      mariadb-client-10.1 && \
    apt-get clean

ENV APP_ROOT="/opt/trinity"
ENV PATH=${PATH}:/opt/trinity/bin

COPY extractmaps.sh ${APP_ROOT}/bin/

RUN chgrp -R 0 ${APP_ROOT} && \
    chmod -R ug+rwx ${APP_ROOT}

USER 1001
WORKDIR ${APP_ROOT}

#EXPOSE 8085/tcp 3724/tcp 3443/tcp 7878/tcp
VOLUME ${APP_ROOT}/data

CMD ["/usr/bin/bash"]
