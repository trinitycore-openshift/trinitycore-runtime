#!/bin/bash

set -e

cd data

mapextractor

vmap4extractor

vmap4assembler Buildings vmaps

mmaps_generator --threads 4

rm -rf Buildings

echo "Done!"

